__author__ = 'chris'


class Document(object):
    def __init__(self, path, scanner):
        self.path = path
        self.__file_obj = open(self.path, "r+")

        contents = self.__file_obj.read()
        self.has_trailing_newline = contents[-1] == '\n'
        self.lines = contents.splitlines()

        self.specimens = scanner.scan(self, contents)

    def __del__(self):
        if self.__file_obj:
            self.__file_obj.close()
            self.__file_obj = None