from document import Document

__author__ = 'chris'


class Supervisor(object):
    def __init__(self, scanner, workflow):
        self.__scanner = scanner
        self.__workflow = workflow
        self.__paths = []

    def run(self):
        for path in self.__paths:
            document = Document(path, self.__scanner)

            if not document.specimens:
                continue

